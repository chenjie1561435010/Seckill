CREATE DATABASE IF NOT EXISTS `low_level_miaosha` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE low_level_miaosha;

CREATE TABLE `goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT comment '商品ID',
  `goods_name` varchar(48) DEFAULT NULL comment '商品名称',
  `goods_title` varchar(192) DEFAULT NULL comment '商品标题',
  `goods_img` varchar(192) DEFAULT NULL comment '商品的图片',
  `goods_detail` longtext DEFAULT NULL comment '商品的详细介绍',
  `goods_price` decimal(10,2) DEFAULT NULL comment '商品单价',
  `goods_stock` int(11) DEFAULT NULL comment '商品库存, -1表示没有限制',
PRIMARY KEY `PRIMARY` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `miaosha_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(20) DEFAULT NULL comment '商品ID',
  `miaosha_price` decimal(10,2) DEFAULT NULL comment '秒杀价',
  `stock_count` int(11) DEFAULT NULL comment '秒杀的库存数量',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '秒杀开始时间',
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '秒杀结束时间',
PRIMARY KEY `PRIMARY` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `miaosha_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT comment '用户ID',
  `user_id` bigint(20) DEFAULT NULL comment '用户ID',
  `order_id` bigint(20) DEFAULT NULL comment '订单ID',
  `goods_id` bigint(20) DEFAULT NULL comment '商品ID',
PRIMARY KEY `PRIMARY` (`id`),
UNIQUE KEY `uid_gid_index` (`user_id`,`goods_id`) USING BTREE
) ENGINE=InnoDB;


CREATE TABLE `order_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL comment '用户ID',
  `goods_id` bigint(20) DEFAULT NULL comment '商品ID',
  `delivery_addr_id` bigint(20) DEFAULT NULL comment '收货地址ID',
  `goods_name` varchar(48) DEFAULT NULL comment '冗余过来的商品名称',
  `goods_count` int(11) DEFAULT NULL comment '商品数量',
  `goods_price` decimal(10,2) DEFAULT NULL comment '商品单价',
  `order_channel` tinyint(4) DEFAULT NULL comment '1pc, 2android, 3ios',
  `status` tinyint(4) DEFAULT NULL comment '订单状态, 0新建未支付, 1已支付, 2已发货, 3已收货, 4已退款, 5已完成',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '订单的创建时间',
  `pay_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '支付时间',
PRIMARY KEY `PRIMARY` (`id`)
) ENGINE=InnoDB;


CREATE TABLE `user` (
  `id` bigint(20) NOT NULL comment '用户ID, 手机号码',
  `nickname` varchar(765) NOT NULL,
  `password` varchar(96) DEFAULT NULL comment 'MD5(MD5(明文密码+固定salt),salt)',
  `salt` varchar(30) DEFAULT NULL,
  `head` varchar(384) DEFAULT NULL comment '头像, 云存储的地址',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '注册时间',
  `last_login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '上次登录时间',
  `login_count` int(11) DEFAULT NULL comment '登录次数',
PRIMARY KEY `PRIMARY` (`id`)
) ENGINE=InnoDB;